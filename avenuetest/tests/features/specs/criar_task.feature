#language: pt

Funcionalidade: Verificar se APP  cria tasks com tamanhos diferentes

Cenário: Criar task com valor de caracteres inferior ao permitido
Quando entro com valor inferior ao permitido
Então verifico se o sistema CRIOU a task ou enviou ALERT


Cenário: Criar task com valor de caracteres superior ao permitido
Quando entro com valor superior ao permitido
Então verifico se o sistema CRIOU a task ou enviou ALERT


