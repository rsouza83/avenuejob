Quando("entro com valor inferior ao permitido") do
    @acessando = AcessandoLogin.new
    @acessando.load
    @acessando.preencher
    click_button 'Sign in'
  find('a[role="button"]').click
  find('#new_task').set 'rc'
  find('#new_task').native.send_keys(:enter)
  
  sleep(5)
  end
  
  Então("verifico se o sistema CRIOU a task ou enviou ALERT") do
    expect(page).to have_content 'rc'
    puts 'Task criada, inferior a 3 caracteres'
  end
  
  Quando("entro com valor superior ao permitido") do
    @acessando = AcessandoLogin.new
    @acessando.load
    @acessando.preencher
    click_button 'Sign in'
    find('a[role="button"]').click
    find('#new_task').set 'Dignissim diam quis enim lobortis. Tempus urna et pharetra pharetra. Lobortis scelerisque fermentum dui faucibus.Odio euismod lacinia at quis risus sed vulputate odio. Faucibus vitae aliquet nec ullamcorper sit amet risus nullam eget. Nisl purus in mollis nunc sed id semper risus. Nulla facilisi etiam dignissim diam quis enim lobortis scelerisque fermentum.'
    find('#new_task').native.send_keys(:enter) 
    expect(page).to have_content 'Nulla facilisi'
    puts 'A task criada, ultrapassa 250 caracteres'

  end
  